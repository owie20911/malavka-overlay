# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg unpacker

MY_PN="code"
PV_EXTRA="1665614327"

DESCRIPTION="Microsoft Visual Studio Code"
HOMEPAGE="https://code.visualstudio.com/"
SRC_URI="https://packages.microsoft.com/repos/code/pool/main/${MY_PN:0:1}/${MY_PN}/${MY_PN}_${PV}-${PV_EXTRA}_amd64.deb"

LICENSE="MIT"
SLOT="0"
KEYWORDS="-* ~amd64"

RESTRICT="bindist mirror strip"

DEPEND=""
RDEPEND="${DEPEND}
	dev-libs/nss
	app-crypt/gnupg
	app-crypt/libsecret
	x11-libs/libXScrnSaver
	media-libs/mesa
	x11-libs/gtk+:3"
BDEPEND=""

PATCHES=(
	"${FILESDIR}/${PN}-fix_desktop_files.patch"
)

S="${WORKDIR}/usr/share"

src_install() {
	dodir /opt/microsoft
	cp -R code "${D}"/opt/microsoft/code || die
	dosym ../../opt/microsoft/code/bin/code /usr/bin/code
	doicon pixmaps/com.visualstudio.code.png
	domenu applications/code.desktop applications/code-url-handler.desktop
	insinto /usr/share/appdata
	doins appdata/code.appdata.xml
	insinto /usr/share/mime/packages
	doins mime/packages/code-workspace.xml
	insinto /usr/share/bash-completion/completions
	doins bash-completion/completions/code
	insinto /usr/share/zsh/site-functions
	doins zsh/vendor-completions/_code
}
