# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

if [ "${PV}" == "9999" ]; then
	VCS_ECLASS="git-r3"
fi

inherit meson ${VCS_ECLASS}

DESCRIPTION="The desktop application to configure Touchegg"
HOMEPAGE="https://github.com/JoseExposito/touche"

if [ "${PV}" == "9999" ]; then
	EGIT_REPO_URI="https://github.com/JoseExposito/${PN}.git"
	KEYWORDS=""
else
	SRC_URI="https://github.com/JoseExposito/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

RESTRICT="mirror"

LICENSE="GPL-3+"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}
	dev-libs/gjs
	dev-libs/glib
	net-libs/nodejs
	dev-libs/gobject-introspection
	x11-libs/gtk+:3
	x11-misc/touchegg
"
BDEPEND=""

src_prepare() {
	npm install
	eapply_user
}
